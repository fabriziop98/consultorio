package com.fabrizio.consultorio.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fabrizio.consultorio.app.models.service.AmazonUpload;

	
	@RestController
	@RequestMapping(value= "/s3")
	public class AmazonS3Controller {

		@Autowired
		private AmazonUpload amazonUpload;

		@PostMapping(value= "/upload")
		public String uploadFile(@RequestPart(value= "file") final String archivo) {
			final MultipartFile multipartFile = amazonUpload.convertirSubir(archivo);
			amazonUpload.uploadFile(multipartFile);
			final String response = "[" + multipartFile.getOriginalFilename() + "] uploaded successfully.";
			return "";
		}
	}


