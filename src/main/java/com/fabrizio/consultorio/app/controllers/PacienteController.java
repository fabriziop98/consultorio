package com.fabrizio.consultorio.app.controllers;


import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fabrizio.consultorio.app.models.entity.Paciente;
import com.fabrizio.consultorio.app.models.entity.Pdf;
import com.fabrizio.consultorio.app.models.entity.Terapeuta;
import com.fabrizio.consultorio.app.models.entity.Turno;
import com.fabrizio.consultorio.app.models.entity.Usuario;
import com.fabrizio.consultorio.app.models.service.AmazonUpload;
import com.fabrizio.consultorio.app.models.service.IPacienteService;
import com.fabrizio.consultorio.app.models.service.IPdfService;
import com.fabrizio.consultorio.app.models.service.ITerapeutaService;
import com.fabrizio.consultorio.app.models.service.ITurnoService;
import com.fabrizio.consultorio.app.models.service.IUploadFileService;
import com.fabrizio.consultorio.app.models.service.IUsuarioService;

import static com.fabrizio.util.Texto.TITULO_LABEL;
import static com.fabrizio.util.Texto.ERROR_LABEL;
import static com.fabrizio.util.Texto.SUCCESS_LABEL;
import static com.fabrizio.util.Texto.TURNO_LABEL;
import static com.fabrizio.util.Texto.TURNOS_LABEL;
import static com.fabrizio.util.Texto.PACIENTE_LABEL;
import static com.fabrizio.util.Texto.PACIENTES_LABEL;
import static com.fabrizio.util.Texto.INFO_LABEL;
import static com.fabrizio.util.Texto.TERAPEUTAS_LABEL;

@Controller
@RequestMapping("/paciente")
public class PacienteController {
	

	@Autowired
	private IPacienteService pacienteService;
	
	@Autowired
	private IPdfService pdfService;
	
	@Autowired
	private ITerapeutaService terapeutaService;
	
	@Autowired
	private IUploadFileService uploadFileService;
	
	@Autowired
	private ITurnoService turnoService;
	
	@Autowired
	private IUsuarioService usuarioService;
	
	@Autowired
	private AmazonUpload amazonUpload;
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA')")
	@GetMapping("/listar")
	public String listarPacientes(Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		List<Paciente> pacientes = pacienteService.findAll();
		
		model.addAttribute(TITULO_LABEL, "Pacientes");
		model.addAttribute(PACIENTES_LABEL, pacientes);
		return PACIENTES_LABEL;
		
	}
	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA')")
	@PostMapping("/turno/{id}")
	public String asignarTurno(@Valid Turno turno, @PathVariable(value="id") Long idPaciente, Model model,
			@RequestParam(name = TURNOS_LABEL, required = false) String turnoString, 
			RedirectAttributes flash, SessionStatus status) {
		try {
			turnoService.crear(turno, idPaciente, turnoString);
		} catch (Exception e) {
			model.addAttribute(TITULO_LABEL, "Asignar turno");
			model.addAttribute(ERROR_LABEL, "Error: Ha ocurrido un error al crear el turno");
			flash.addFlashAttribute(ERROR_LABEL, "Error: Ha ocurrido un error al crear el turno");
			e.printStackTrace();
			return "redirect:/paciente/ver/{id}";
		}
		flash.addFlashAttribute(SUCCESS_LABEL, "Turno asignado: "+turnoString+", con terapeuta: "+turno.getTerapeuta().getUsername()+" "+turno.getTerapeuta().getApellido());
		status.setComplete();
		return "redirect:/paciente/ver/{id}";
	}
	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@PostMapping("/form/{id}")
	public String asignarTerapeuta(@PathVariable(value = "id") Long id, Paciente pacienteModelo, Model model,
			RedirectAttributes flash, SessionStatus status) {
		
		model.addAttribute(PACIENTES_LABEL, pacienteModelo);
		
		if (pacienteModelo.getTerapeutas() == null || pacienteModelo.getTerapeutas().isEmpty()) {
			model.addAttribute(TITULO_LABEL, "Asignar terapeuta");
//			el error se conecta al th:errorclass
			flash.addFlashAttribute(ERROR_LABEL, "Error: El terapeuta es nulo");
			return "asignarTerapeuta";
		}
		
		try {
			pacienteService.asignarTerapeuta(pacienteModelo, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		flash.addFlashAttribute(SUCCESS_LABEL, "Terapeuta asignado con éxito");
		
		
		
		return "redirect:/paciente/ver/{id}";
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA')")
	@GetMapping("/listar/{id}")
	public String verPacienteTerapeuta(@PathVariable(value="id") Long id, Model model, Model modelo, RedirectAttributes flash) {
		List<Paciente> pacientes = pacienteService.findByTerapeutaId(id);
		usuarioService.usuarioEnSesion(model);
		model.addAttribute(TITULO_LABEL, "Pacientes");
		model.addAttribute(PACIENTES_LABEL, pacientes);
		modelo.getAttribute("dateTimePicker");
		return PACIENTES_LABEL;
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_PACIENTE','ROLE_TERAPEUTA')")
	@GetMapping(value = "/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id,  Model model, RedirectAttributes flash) {
		Paciente paciente = pacienteService.findOne(id);
		if (paciente == null) {
			flash.addFlashAttribute(ERROR_LABEL, "El cliente no existe en la base de datos");
			return "redirect:/paciente/listar";
		}
		usuarioService.usuarioEnSesion(model); 
		Turno turno = new Turno();
		List<Terapeuta> terapeutas = paciente.getTerapeutas();
		List<Terapeuta> terapeutasDisponibles = terapeutaService.listarDisponibles(paciente);
		model.addAttribute(TURNO_LABEL, turno);
		model.addAttribute(PACIENTE_LABEL, paciente);
		model.addAttribute(TERAPEUTAS_LABEL, terapeutas);
		model.addAttribute("terapeutasDisponibles", terapeutasDisponibles);
		model.addAttribute(TITULO_LABEL, "Detalle paciente: " + paciente.getUsername() +" "+ paciente.getApellido());
		model.addAttribute("nombreTerapeuta", paciente.getTerapeutas().toString().replace("[", "").replace("]", ""));
		model.addAttribute(TURNOS_LABEL, turnoService.listarSorted(turnoService.listarFuturos(paciente)));
		return "verPaciente";
	}
	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_PACIENTE','ROLE_TERAPEUTA')")
	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename) {
		Resource recurso = null;
		try {
			recurso = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
				.body(recurso);
	}
	
	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA')")
	@PostMapping("/pdf/{id}")
	public String subirPdf(@PathVariable Long id, Model model, @RequestParam("file") MultipartFile archivo, RedirectAttributes flash, SessionStatus status) {
		Paciente paciente = pacienteService.findOne(id);
		Pdf pdf = new Pdf();

		if(archivo.isEmpty()) {
			model.addAttribute(TITULO_LABEL, "Subir informe");
			model.addAttribute(ERROR_LABEL, "Error: El informe no puede ser nulo.");
			return "inicio";
		}
		
		if(!archivo.isEmpty()) {
			String uniqueFilename = null;
			uniqueFilename = amazonUpload.uploadFileDoc(archivo);
//			try {
//				uniqueFilename = uploadFileService.copy(archivo);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			flash.addFlashAttribute(INFO_LABEL, "Se ha subido correctamente el pdf: "+archivo.getOriginalFilename());
			pdf.setNombre(uniqueFilename);
			paciente.addPdf(pdf);
			pdf.setFechaSubida(new Date());
			pdf.setEliminado(false);
		}
		pacienteService.save(paciente);
		status.setComplete();
		
		return "redirect:/paciente/ver/{id}";
	}
	
//	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_PACIENTE','ROLE_TERAPEUTA')")
//	@GetMapping(value = "/listado/pdf/{filename:.+}")
//	public ResponseEntity<Resource> verPdf(@PathVariable String filename) {
//		Resource recurso = null;
//		try {
//			amazonUpload.getArchivo(filename);
//			recurso = uploadFileService.load(filename);
//		} 
//		catch (MalformedURLException e) {
//			e.printStackTrace();
//		}
//
//		return ResponseEntity.ok()
//				.body(recurso);
//	}
	
	 @GetMapping(value= "/listado/pdf/{filename:.+}")
	    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String filename) {
	        final byte[] data = amazonUpload.downloadFile(filename);
	        final ByteArrayResource resource = new ByteArrayResource(data);
	        return ResponseEntity
	                .ok()
	                .contentLength(data.length)
	                .header("Content-type", "application/octet-stream")
	                .header("Content-disposition", "attachment; filename=\"" + filename + "\"")
	                .body(resource);
	    }
	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@RequestMapping(value = "/pdf/eliminar/{idPdf}/{idPaciente}")
	public String eliminarPdf(@PathVariable(value = "idPdf") Long id,@PathVariable(value = "idPaciente") Long idPaciente,  RedirectAttributes flash, Model model) {
		if (id>0){
			Pdf pdf = pdfService.findOne(id);
			pdfService.darDeBaja(pdf);
			flash.addFlashAttribute(SUCCESS_LABEL, "Informe: "+(pdf.getNombre()).substring(pdf.getNombre().lastIndexOf("_") + 1)+" eliminado.");
			System.out.println(model.getAttribute(PACIENTE_LABEL));
		}
		return "redirect:/paciente/listadoPdf/"+idPaciente;
	}
	
	@GetMapping("/foto/{id}")
    public ResponseEntity<byte[]> abrirFoto(@PathVariable(name = "id") Long id) {
		final HttpHeaders headers = new HttpHeaders();
		Paciente paciente = pacienteService.findOne(id);
		final byte[] data = amazonUpload.downloadFile(paciente.getFoto());
		
		InputStream is = new BufferedInputStream(new ByteArrayInputStream(data));
		String mimeType = null;
		try {
			mimeType = URLConnection.guessContentTypeFromStream(is);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (paciente.getFoto() != null) {

			String extension = mimeType;

			if (extension.equals("image/jpeg")) {
				MediaType media = MediaType.parseMediaType("image/jpeg");
				headers.setContentType(media);
			} else if (extension.equals("image/png")) {
				headers.setContentType(MediaType.IMAGE_PNG);
			} else {
				headers.setContentType(MediaType.IMAGE_JPEG);
			}

			return new ResponseEntity<>(data, headers, HttpStatus.OK);
		} else {
			return null;
		}
    }
	
//	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_PACIENTE','ROLE_TERAPEUTA')")
//	@GetMapping("/foto/{id}")
//	public ResponseEntity<byte[]> abrirFoto(@PathVariable(name = "id") Long id) {
//		final HttpHeaders headers = new HttpHeaders();
//
//		Paciente paciente = pacienteService.findOne(id);
//
//		if (paciente.getFoto() != null) {
//
//			String path = paciente.getFoto();
//
//			File file = new File(path);
//			String extension = getFileExtension(file);
//
//			if (extension.equals(".jpeg")) {
//				MediaType media = MediaType.parseMediaType("image/jpeg");
//				headers.setContentType(media);
//			} else if (extension.equals(".png")) {
//				headers.setContentType(MediaType.IMAGE_PNG);
//			} else {
//				headers.setContentType(MediaType.IMAGE_JPEG);
//			}
//
//			return new ResponseEntity<>(readFileToByteArray(file), headers, HttpStatus.OK);
//		} else {
//			return null;
//		}
//
//	}
//
//	private String getFileExtension(File file) {
//		String name = file.getName();
//		int lastIndexOf = name.lastIndexOf(".");
//		if (lastIndexOf == -1) {
//			return "";
//		}
//		return name.substring(lastIndexOf);
//	}
//	
//	private static byte[] readFileToByteArray(File file) {
//		FileInputStream fis;
//		byte[] bArray = new byte[(int) file.length()];
//		try {
//			fis = new FileInputStream(file);
//			fis.read(bArray);
//			fis.close();
//
//		} catch (IOException ioExp) {
//			ioExp.printStackTrace();
//		}
//		return bArray;
//	}
//	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@RequestMapping(value = "/eliminar")
	public String eliminar(RedirectAttributes flash) {

		pacienteService.deleteAll();
		flash.addFlashAttribute(SUCCESS_LABEL, "Todas las terapeutas fueron eliminadas con éxito");

		return "redirect:/receta/listar";
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
		if (id>0){
			Paciente paciente = pacienteService.findOne(id);
			pacienteService.darDeBaja(paciente);
			flash.addFlashAttribute(SUCCESS_LABEL, "Terapeuta: "+paciente.getApellido()+" dado de baja.");
			
		}
		return "redirect:/terapeuta/listar";
	}
	
}
