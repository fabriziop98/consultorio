package com.fabrizio.consultorio.app.controllers;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fabrizio.consultorio.app.models.entity.Paciente;
import com.fabrizio.consultorio.app.models.entity.Turno;
import com.fabrizio.consultorio.app.models.service.IPacienteService;
import com.fabrizio.consultorio.app.models.service.ITurnoService;
import com.fabrizio.consultorio.app.models.service.IUsuarioService;

import static com.fabrizio.util.Texto.TITULO_LABEL;
import static com.fabrizio.util.Texto.TURNO_LABEL;
import static com.fabrizio.util.Texto.TURNOS_LABEL;
import static com.fabrizio.util.Texto.PACIENTE_LABEL;
import static com.fabrizio.util.Texto.INFO_LABEL;
@Controller
@RequestMapping("/"+TURNO_LABEL)
public class TurnoController {

	@Autowired
	private ITurnoService turnoService;
	
	@Autowired
	private IPacienteService pacienteService;
	
	@Autowired
	private IUsuarioService usuarioService;
	
	
	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@GetMapping("/listar")
	public String listarTurnos(Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		model.addAttribute(TITULO_LABEL, "Todos los turnos");
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarTodosActivos()));
		
		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA','ROLE_PACIENTE')")
	@GetMapping("/listar/{id}")
	public String listarTurnosPorPaciente(@PathVariable Long id, Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		Paciente paciente = pacienteService.findOne(id);
		
		model.addAttribute(TITULO_LABEL, "Todos los turnos de: "+paciente.getApellido() + " " + paciente.getUsername());
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarTodosActivos(paciente)));
		model.addAttribute(PACIENTE_LABEL, paciente);

		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@GetMapping("/listarAnteriores")
	public String listarTurnosAnteriores(Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		model.addAttribute(TITULO_LABEL, "Todos los turnos pasados.");
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarPasados()));
		
		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA','ROLE_PACIENTE')")
	@GetMapping("/listarAnteriores/{id}")
	public String listarTurnosAnteriores(@PathVariable Long id, Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		Paciente paciente = pacienteService.findOne(id);
		usuarioService.usuarioEnSesion(model);
		model.addAttribute(TITULO_LABEL, "Turnos pasados de: "+paciente.getApellido() + " " + paciente.getUsername());
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarPasados(paciente)));
		model.addAttribute(PACIENTE_LABEL, paciente);
		
		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@GetMapping("/listarFuturos")
	public String listarTurnosFuturos(Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		List<Turno> turnos = turnoService.listarSortedObject(turnoService.listarFuturos());
		
		model.addAttribute(TITULO_LABEL, "Turnos futuros");
		model.addAttribute(TURNOS_LABEL, turnos);
		
		
		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA','ROLE_PACIENTE')")
	@GetMapping("/listarFuturos/{id}")
	public String listarTurnosFuturos(@PathVariable Long id, Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		Paciente paciente = pacienteService.findOne(id);
		
		model.addAttribute(TITULO_LABEL, "Turnos futuros de: "+paciente.getApellido() + " " + paciente.getUsername());
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarFuturos(paciente)));
		model.addAttribute(PACIENTE_LABEL, paciente);
		
		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA','ROLE_PACIENTE')")
	@GetMapping("/listarEliminados")
	public String listarTurnosEliminados(Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		model.addAttribute(TITULO_LABEL, "Todos los turnos eliminados");
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarEliminados()));
		
		
		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA','ROLE_PACIENTE')")
	@GetMapping("/listarEliminados/{id}")
	public String listarTurnosEliminados(@PathVariable Long id, Model model, Authentication authentication, HttpServletRequest request, Locale locale) {
		usuarioService.usuarioEnSesion(model);
		Paciente paciente = pacienteService.findOne(id);
		model.addAttribute(TITULO_LABEL, "Turnos eliminados de: "+paciente.getApellido() + " " + paciente.getUsername());
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarEliminados(paciente)));
		model.addAttribute(PACIENTE_LABEL, paciente);
		
		return TURNOS_LABEL;
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR','ROLE_TERAPEUTA')")
	@GetMapping("/turnosTerapeuta/{id}")
	public String listarTurnosTerapeuta(@PathVariable(value = "id") Long id, Model model) {
		usuarioService.usuarioEnSesion(model);
		model.addAttribute(TITULO_LABEL, "Turnos próximos: ");
		model.addAttribute(TURNOS_LABEL, turnoService.listarSortedObject(turnoService.listarFuturos(turnoService.listarPorTerapeuta(id))));
		return TURNOS_LABEL;
	}
	
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@GetMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		usuarioService.usuarioEnSesion(model);
		Turno turno = turnoService.findOne(id);
		turnoService.darDeBaja(turno);
		model.addAttribute(TITULO_LABEL, "Eliminar turno de: "+turno.getPaciente().getUsername()+" "+turno.getPaciente().getApellido());
		flash.addFlashAttribute(INFO_LABEL, "Eliminar turno de: "+turno.getPaciente().getUsername()+" "+turno.getPaciente().getApellido());
		model.addAttribute(TURNO_LABEL, turno);
		return "eliminarTurno";
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@PostMapping(value = "/eliminar")
	public String eliminar(@Valid Turno turno, Model model, RedirectAttributes flash) {
			flash.addFlashAttribute("success", "Turno de: "+turno.getPaciente().getUsername()+" "+turno.getPaciente().getApellido()+" eliminado.");
			turnoService.darDeBaja(turno);
		return "redirect:/turno/listarEliminados/"+turno.getPaciente().getId();
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR')")
	@RequestMapping(value = "/eliminarTodos")
	public String eliminar(RedirectAttributes flash) {

		turnoService.deleteAll();
		flash.addFlashAttribute("success", "Todas las turnos fueron eliminadas con éxito");

		return "redirect:/receta/listar";
	}
	
	
}
